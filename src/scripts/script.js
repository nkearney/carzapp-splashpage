//=require ../../node_modules/jquery/dist/jquery.js

  $(window).on('load', function() { 
    // Animate loader off screen
    $(".se-pre-con").fadeOut("slow");
});

//=require ../../node_modules/bootstrap-sass/assets/javascripts/bootstrap.js
//=require ../../node_modules/slick-carousel/slick/slick.min.js
//=require ../../node_modules/owl.carousel/dist/owl.carousel.min.js
//=require ../../node_modules/justifiedGallery/dist/js/jquery.justifiedGallery.min.js

// Custom scripts

// Custom.js **** add your custom scripts here

$(document).ready(function () {

    var viewportWidth = $(window).width();
    var bannerheight = $('#carousel-example-generic').height();

    // make dropdown fire on hover
    if (viewportWidth > 1024) {
        $('.dropdown').hover(function () {
            $('.dropdown-toggle', this).trigger('click');
        });
    } else if(viewportWidth < 1024) {
       $('.dropdown').click(function () {
            $('.dropdown-toggle', this).trigger('click');
        });
    }

    $(window).resize(function () {
        var viewportWidth = $(window).width();
        if (viewportWidth > 1024) {
            $('.dropdown').hover(function () {
                $('.dropdown-toggle', this).trigger('click');
            });
        } else if(viewportWidth < 1024) {
           $('.dropdown').click(function () {
                $('.dropdown-toggle', this).trigger('click');
            });
        }
    })

    $(window).scroll(function () {
        var viewportWidth = $(window).width();
        var bannerheight = $('#carousel-example-generic').height();
        var menuHeight = 100;
        if(viewportWidth > 768){
            $('.navbar').toggleClass("light", ($(window).scrollTop() > menuHeight));
        }
    })

    // if mobile view - add white header
    if (viewportWidth < 768) {
        $(".navbar").addClass("light");
    } else if (viewportWidth > 768 && ($(window).scrollTop() < bannerheight)) {
        $('.navbar').removeClass('light');
    }

    $(window).resize(function () {
        var viewportWidth = $(window).width();
        var bannerheight = $('#carousel-example-generic').height();
        if (viewportWidth < 768) {
            $(".navbar").addClass("light");
        } else if (viewportWidth > 768 && ($(window).scrollTop() < bannerheight)) {
            $('.navbar').removeClass('light');
        }
    })

});

$('.owl-carousel').owlCarousel({
    loop:true,
    margin:10,
    lazyLoad:true,
    nav:false,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            items:4
        }
    }
});



/* Demo Scripts for Bootstrap Carousel and Animate.css article
 * on SitePoint by Maria Antonietta Perna
 */
(function ($) {

    //Function to animate slider captions 
    function doAnimations(elems) {
        //Cache the animationend event in a variable
        var animEndEv = 'webkitAnimationEnd animationend';

        elems.each(function () {
            var $this = $(this),
                $animationType = $this.data('animation');
            $this.addClass($animationType).one(animEndEv, function () {
                $this.removeClass($animationType);
            });
        });
    }

    //Variables on page load 
    var $myCarousel = $('#carousel-example-generic'),
        $firstAnimatingElems = $myCarousel.find('.item:first').find("[data-animation ^= 'animated']");

    //Initialize carousel 
    $myCarousel.carousel();

    //Animate captions in first slide on page load 
    doAnimations($firstAnimatingElems);

    //Pause carousel  
    $myCarousel.carousel('pause');


    //Other slides to be animated on carousel slide event 
    $myCarousel.on('slide.bs.carousel', function (e) {
        var $animatingElems = $(e.relatedTarget).find("[data-animation ^= 'animated']");
        doAnimations($animatingElems);
    });

})(jQuery);


$("#main-nav a").click(function(e){       
    e.preventDefault();
    $('html,body').animate({scrollTop:$(this.hash).offset().top-100}, "slow");
});

$(".button-scroll").click(function(e){       
    e.preventDefault();
    $('html,body').animate({scrollTop:$(this.hash).offset().top-100}, "slow");
});

$(".scroll-top a").click(function(e){       
    e.preventDefault();
    $('html,body').animate({scrollTop:0},"slow")
});

$(document).scroll(function() {
    var y = $(this).scrollTop();
    if (y > 600) {
      $('.scroll-top').fadeIn();
    } else {
      $('.scroll-top').fadeOut();
    }
  });


